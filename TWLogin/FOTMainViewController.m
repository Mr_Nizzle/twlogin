//
//  FOTMainViewController.m
//  TWLogin
//
//  Created by Ricardo Guillen on 10/1/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import "FOTMainViewController.h"
#import "FOTAppDelegate.h"

@interface FOTMainViewController ()

@end

@implementation FOTMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:@"Logout"
                                              style:UIBarButtonItemStyleBordered
                                              target:self
                                              action:@selector(logoutButtonWasPressed:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)logoutButtonWasPressed:(id)sender {
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:NO forKey:@"TWUserIsLoggedIn"];
    NSLog(@"a ver %d", [defaults boolForKey:@"TWUserIsLoggedIn"]);
    FOTAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate checkLoginStatus];
}

-(void)viewWillAppear:(BOOL)animated{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *accountInfo = [defaults objectForKey:@"TWUserAccountInfo"];
    
    [[self userNameLabel] setText:[NSString stringWithFormat:@"@%@",[accountInfo valueForKey:@"user_name"]]];
}

@end
