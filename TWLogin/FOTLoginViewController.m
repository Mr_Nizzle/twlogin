//
//  FOTLoginViewController.m
//  TWLogin
//
//  Created by Ricardo Guillen on 10/1/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import "FOTLoginViewController.h"
#import <Accounts/Accounts.h>
#import <Social/Social.h>
#import "FOTAppDelegate.h"

@interface FOTLoginViewController () <UIAlertViewDelegate>

@end

@implementation FOTLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[self loginActivityIndicator] setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)performTWLogin:(id)sender {
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    [self.loginActivityIndicator startAnimating];
    [[self loginActivityIndicator] setHidden:NO];
    
    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account
                                  accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    [account requestAccessToAccountsWithType:accountType
                                     options:nil completion:^(BOOL granted, NSError *error)
     {
         if (granted == YES)
         {
             NSArray *arrayOfAccounts = [account
                                         accountsWithAccountType:accountType];
             NSLog(@"accounts = %@", arrayOfAccounts);
             if ([arrayOfAccounts count] > 0)
             {
                 ACAccount *twitterAccount = [arrayOfAccounts lastObject];
                 NSLog(@"ACCOUNT %@", twitterAccount);
                 NSDictionary *accountInfo = @{
                                               @"user_name": [twitterAccount valueForKey:@"username"],
                                               @"user_id": [[twitterAccount valueForKey:@"properties"] valueForKey:@"user_id"]
                                               };
                 
                 NSLog(@"ACCOUNT INFO %@", accountInfo);
                 NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setObject:accountInfo forKey:@"TWUserAccountInfo"];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                     [self.loginActivityIndicator stopAnimating];
                     [[self loginActivityIndicator] setHidden:YES];
                     NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                     [defaults setBool:YES forKey:@"TWUserIsLoggedIn"];
                     NSLog(@"a ver %d", [defaults boolForKey:@"TWUserIsLoggedIn"]);
                     FOTAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
                     [appDelegate enterApplication];
                 });
             }
             else{
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                     [self.loginActivityIndicator stopAnimating];
                     [[self loginActivityIndicator] setHidden:YES];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TW Login" message:@"No tienes una cuenta de Twitter asociada con tu móvil, puedes configurarla en Settings > Twitter." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                 });
             }
         } else {
             // Handle failure to get account access
             NSLog(@"fallo %d", [error code]);
             
             if([error code] == 6){
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                     [self.loginActivityIndicator stopAnimating];
                     [[self loginActivityIndicator] setHidden:YES];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TW Login" message:@"No tienes una cuenta de Twitter asociada con tu móvil, puedes configurarla en Settings > Twitter." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                 });
             }
             else{
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                     [self.loginActivityIndicator stopAnimating];
                     [[self loginActivityIndicator] setHidden:YES];
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"TW Login" message:@"Debes permitirle el acceso a tu cuenta de Twitter a esta aplicación en Settings > Privacy > Twitter." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                     [alert show];
                 });
             }
             
         }
     }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
}



@end
