//
//  FOTLoginViewController.h
//  TWLogin
//
//  Created by Ricardo Guillen on 10/1/13.
//  Copyright (c) 2013 503Estudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FOTLoginViewController : UIViewController

- (IBAction)performTWLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loginActivityIndicator;
@end
